vim.keymap.set('n', '<cr>', "<c-]>", { buffer=true, noremap = true })
vim.keymap.set('n', 'o',    "<c-]>", { buffer=true, noremap = true })
vim.keymap.set('n', '<bs>', "<c-T>", { buffer=true, noremap = true })
