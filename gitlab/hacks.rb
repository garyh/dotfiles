Mr = MergeRequest

unless ENV["FOSS_ONLY"] == "1"
Amr = ApprovalMergeRequestRule
Apr = ApprovalProjectRule
Ars = ApprovalMergeRequestRuleSource
end

def dbg(b_ing = binding)
  require 'pry'
  b_ing.pry
end

Rails.logger.level = Logger::WARN
